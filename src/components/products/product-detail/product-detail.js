import React, { Component } from "react";

class ProductDetail extends Component {
  toggleProductModal = () => {
    const { toggleProductModal } = this.props;
    toggleProductModal(this.props.product);
  };

  render() {
    const { product } = this.props;
    return (
      <div className="modal-container" id="productModal">
        <div className="modal-content">
          <div className="close-label" onClick={this.toggleProductModal}>
            <i className="fas fa-times" alt="Cerrar"></i>
          </div>
          <h3 className="orange fw-bold">{product?.name}</h3>
          <img src={product?.image} />
          <div
            className="product-description"
            dangerouslySetInnerHTML={{
              __html: product?.description,
            }}
          />
        </div>
      </div>
    );
  }
}

export default ProductDetail;
