import React, { Component } from "react";
import ProductCard from "./components/product-card";
import InfiniteScroll from "react-infinite-scroll-component";
import "./products-list.css";

class ProductsListContainer extends Component {
  render() {
    const {
      error,
      isLoaded,
      itemsToShow,
      fetchMoreData,
      hasMore,
      addProductToCart,
      toggleProductModal,
    } = this.props;
    if (error) {
      return <div>Error: {error.message}</div>;
    } else if (!isLoaded) {
      return (
        <div className="d-flex justify-content-center align-items-center vh-100">
          <img
            className="load-image"
            src={`${process.env.PUBLIC_URL}/load.gif`}
          ></img>
        </div>
      );
    } else {
      return (
        <InfiniteScroll
          dataLength={itemsToShow.length}
          next={fetchMoreData}
          hasMore={hasMore}
          loader={
            <div className="d-flex justify-content-center align-items-center vh-30 mb-5">
              <img
                className="load-image"
                src={`${process.env.PUBLIC_URL}/load.gif`}
              ></img>
            </div>
          }
        >
          <div className="container mb-5 pb-5">
            <div className="row mt-5 pt-5">
              {itemsToShow.map((item) => (
                <ProductCard
                  product={item}
                  key={item.id}
                  addProductToCart={addProductToCart}
                  toggleProductModal={toggleProductModal}
                ></ProductCard>
              ))}
            </div>
          </div>
        </InfiniteScroll>
      );
    }
  }
}

export default ProductsListContainer;
