import React, { Component } from "react";
import classNames from "classnames";

class ProductCard extends Component {
  addProductToCart = () => {
    this.props.addProductToCart(this.props.product);
  };

  renderLabel(stock) {
    if (stock <= 5 && stock > 0)
      return (
        <span className="badge bg-warning text-dark">
          ¡Últimas unidades en stock!
        </span>
      );
    if (stock <= 0) return <span className="badge bg-danger ">Sin stock </span>;
  }

  toggleProductModal = () => {
    const { toggleProductModal } = this.props;
    toggleProductModal(this.props.product);
  };

  render() {
    return (
      <div className="col-sm-12 col-md-6 col-xl-3 mt-4">
        <div
          className={classNames("product-card", "fw-bold", {
            disabled: this.props.product.stock <= 0,
          })}
        >
          <div onClick={this.toggleProductModal}>
            <div className="image">
              <img src={this.props.product.image} />
              {this.renderLabel(this.props.product.stock)}
            </div>
            <h6 className="product-name">{this.props.product.name}</h6>
            <div
              className="product-description"
              dangerouslySetInnerHTML={{
                __html:
                  this.props.product.description.substr(0, 150).concat("...") +
                  "</p>",
              }}
            />
          </div>
          <div>
            <p className="fw-bold product-price">
              {this.props.product.price} €
            </p>
            <button
              disabled={this.props.product.stock <= 0}
              onClick={this.addProductToCart}
              className="btn bg-orange w-100 text-black fw-bold"
            >
              <i className="fas fa-cart-plus"></i>
            </button>
          </div>
        </div>
      </div>
    );
  }
}

export default ProductCard;
