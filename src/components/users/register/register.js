import React, { Component } from "react";

class RegisterComponent extends Component {
  register = (event) => {
    event.preventDefault();
    const name = document.getElementById("registerName").value;
    const email = document.getElementById("registerEmail").value;
    const password = document.getElementById("registerPassword").value;
    if (
      !name ||
      name === "" ||
      !email ||
      email === "" ||
      !password ||
      password === ""
    )
      alert("Por favor rellene el formulario");
    else this.props.register(name, email, password);
  };

  render() {
    const { toggleRegister } = this.props;

    return (
      <div className="modal-container" id="registerContainer">
        <div className="modal-content h-75 w-50 d-flex align-items-center justify-content-center">
          <form className="w-100 h-100 d-flex align-items-center flex-column justify-content-around">
            <div className="close-label w-100" onClick={toggleRegister}>
              <i className="fas fa-times" alt="Cerrar"></i>
            </div>
            <div className="mb-3  w-100">
              <label
                htmlFor="name"
                className="form-label text-black fw-bold  w-100"
              >
                Nombre
              </label>
              <input
                type="name"
                className="form-control  w-100"
                id="registerName"
                aria-describedby="nameHelp"
              />
            </div>
            <div className="mb-3  w-100">
              <label
                htmlFor="email"
                className="form-label text-black fw-bold  w-100"
              >
                Email
              </label>
              <input
                type="email"
                className="form-control  w-100"
                id="registerEmail"
                aria-describedby="emailHelp"
              />
            </div>
            <div className="mb-3  w-100">
              <label
                htmlFor="password"
                className="form-label text-black fw-bold  w-100"
              >
                Contraseña
              </label>
              <input
                type="password"
                className="form-control  w-100"
                id="registerPassword"
                autoComplete="on"
              />
            </div>
            <button
              type="submit"
              className="btn bg-orange w-100 text-black fw-bold"
              onClick={this.register}
            >
              Registrarse
            </button>
          </form>
        </div>
      </div>
    );
  }
}

export default RegisterComponent;
