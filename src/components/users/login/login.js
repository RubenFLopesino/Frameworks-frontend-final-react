import React, { Component } from "react";

class LoginComponent extends Component {
  login = (event) => {
    event.preventDefault();
    const email = document.getElementById("email").value;
    const password = document.getElementById("password").value;
    if (!email || email === "" || !password || password === "")
      alert("Por favor rellene el formulario");
    this.props.login(email, password);
  };

  render() {
    const { toggleLogin } = this.props;

    return (
      <div className="modal-container" id="loginContainer">
        <div className="modal-content h-75 w-50 d-flex align-items-center justify-content-center">
          <form className="w-100 h-100 d-flex align-items-center flex-column justify-content-around">
            <div className="close-label w-100" onClick={toggleLogin}>
              <i className="fas fa-times" alt="Cerrar"></i>
            </div>
            <div className="mb-3  w-100">
              <label
                htmlFor="email"
                className="form-label text-black fw-bold  w-100"
              >
                Email
              </label>
              <input
                required
                type="email"
                className="form-control  w-100"
                id="email"
                aria-describedby="emailHelp"
              />
            </div>
            <div className="mb-3  w-100">
              <label
                htmlFor="password"
                className="form-label text-black fw-bold  w-100"
              >
                Contraseña
              </label>
              <input
                required
                type="password"
                className="form-control  w-100"
                id="password"
                autoComplete="on"
              />
            </div>
            <button
              type="submit"
              className="btn bg-orange w-100 text-black fw-bold"
              onClick={this.login}
            >
              Iniciar sesión
            </button>
          </form>
        </div>
      </div>
    );
  }
}

export default LoginComponent;
