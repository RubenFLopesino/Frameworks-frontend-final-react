import { Component } from "react";
import axios from "axios";
import ProductsListContainer from "./products/products-list/products-list.container";
import Nav from "./navbar/navbar";
import CartContainer from "./cart/cart";
import ProductDetail from "./products/product-detail/product-detail";
import FinishPurchaseModal from "./cart/finish-purchase/finish-purchase";
import LoginComponent from "./users/login/login";
import jwtDecode from "jwt-decode";
import RegisterComponent from "./users/register/register";
import ProductFormComponent from "./admin/create-product/product-form";
import FeaturedCategories from "./categories/featured-categories/cartegories-container";
import CategoryFormComponent from "./admin/create-category/category-form";
import CategoriesMenuComponent from "./categories/all-categories/all-categories";
import ManageStockComponent from "./admin/stock-management/stock-management";
const TOKEN_KEY = "auth-token";
const PAGE_ELEMENTS = 12;

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      allItems: [],
      items: [],
      itemsToShow: [],
      categories: [],
      hasMore: true,
      cart: {
        id: null,
        totalPrice: 0.0,
        userId: null,
        isActive: true,
        productsToCart: [],
      },
      productSelected: null,
      token: null,
      user: null,
    };
  }

  getRequest = async (url) => {
    return axios.get(url);
  };

  postRequest = async (url, body) => {
    return axios.post(url, body, {
      headers: {
        "x-access-token": this.state?.token ?? "",
      },
    });
  };

  putRequest = async (url, body) => {
    return axios.put(url, body, {
      headers: {
        "x-access-token": this.state?.token ?? "",
      },
    });
  };

  componentDidMount() {
    setTimeout(
      function () {
        this.getRequest(`${process.env.REACT_APP_API_URL}/products`).then(
          (result) => {
            this.setState({
              allItems: result.data,
              items: result.data,
              itemsToShow: result.data.slice(0, PAGE_ELEMENTS),
            });
          }
        );
        this.getRequest(`${process.env.REACT_APP_API_URL}/categories`).then(
          (result) => {
            this.setState({
              categories: result.data,
              isLoaded: true,
            });
          }
        );
      }.bind(this),
      500
    );
  }

  filterByCategory = (categoryId, isFromMenu = false) => {
    let { allItems, itemsToShow } = this.state;
    itemsToShow = allItems.filter(
      (product) => product.category.id === categoryId
    );
    document.body.scrollTop = document.documentElement.scrollTop = 0;
    this.setState({
      items: itemsToShow,
      itemsToShow: itemsToShow.slice(0, PAGE_ELEMENTS),
      hasMore: true,
    });
    if (isFromMenu) {
      this.toggleCategoriesMenu();
    }
  };

  deleteCategoryFilter = () => {
    const { allItems } = this.state;
    const items = allItems;
    const itemsToShow = allItems.slice(0, PAGE_ELEMENTS);
    document.body.scrollTop = document.documentElement.scrollTop = 0;
    this.setState({
      items,
      itemsToShow,
      hasMore: true,
    });
  };

  fetchMoreData = () => {
    const { itemsToShow, items } = this.state;
    const itemsToShowLength = itemsToShow.length;
    const itemsLength = items.length;
    if (itemsToShowLength >= itemsLength) this.setState({ hasMore: false });
    else
      setTimeout(
        function () {
          let startIndex = itemsToShowLength;
          if (startIndex >= itemsLength) startIndex = itemsLength - 1;
          let endIndex = startIndex + PAGE_ELEMENTS;
          if (endIndex >= itemsLength)
            this.setState({
              itemsToShow: itemsToShow.concat(items.slice(startIndex)),
              hasMore: false,
            });
          else
            this.setState({
              itemsToShow: itemsToShow.concat(
                items.slice(startIndex, endIndex)
              ),
              hasMore: true,
            });
        }.bind(this),
        500
      );
  };

  isCartShowed = () => {
    return document.getElementById("cartContainer").classList.contains("show");
  };

  addProductToCart = (product) => {
    const { cart, itemsToShow, user } = this.state;
    let productIndex = cart.productsToCart.findIndex(
      (cartProduct) => cartProduct.product.id === product.id
    );
    if (productIndex >= 0) {
      cart.productsToCart[productIndex].quantity += 1;
    } else {
      cart.productsToCart.push({
        product,
        quantity: 1,
        id: null,
      });
    }
    cart.totalPrice = +cart.totalPrice + +product.price;

    productIndex = itemsToShow.findIndex(
      (productToShow) => productToShow.id === product.id
    );
    if (productIndex >= 0) itemsToShow[productIndex].stock -= 1;
    this.setState({
      cart,
      itemsToShow,
    });
    if (!this.isCartShowed()) {
      this.toggleCart();
      setTimeout(
        function () {
          this.toggleCart();
        }.bind(this),
        1500
      );
    }
    if (user) {
      cart.userId = user.id;
      this.saveCart(cart);
    }
  };

  addProductQuantity = (productToAdd) => {
    const { cart, items, user } = this.state;
    let productIndex = items.findIndex(
      (product) => product.id === productToAdd.id
    );
    if (items[productIndex].stock > 0) {
      items[productIndex].stock -= 1;
      productIndex = cart.productsToCart.findIndex(
        (cartProduct) => cartProduct.product.id === productToAdd.id
      );
      cart.productsToCart[productIndex].quantity += 1;
      cart.totalPrice = +cart.totalPrice + +productToAdd.price;
      this.setState({
        cart,
        items,
      });
    }
    if (user) {
      cart.userId = user.id;
      this.saveCart(cart);
    }
  };

  decreaseProductQuantity = (productToAdd) => {
    const { cart, items, user } = this.state;
    let productIndex = items.findIndex(
      (product) => product.id === productToAdd.id
    );
    items[productIndex].stock += 1;
    productIndex = cart.productsToCart.findIndex(
      (cartProduct) => cartProduct.product.id === productToAdd.id
    );
    cart.productsToCart[productIndex].quantity -= 1;
    cart.totalPrice = +cart.totalPrice - +productToAdd.price;
    if (cart.productsToCart[productIndex].quantity === 0)
      cart.productsToCart.splice(productIndex, 1);

    this.setState({
      cart,
      items,
    });
    if (user) {
      cart.userId = user.id;
      this.saveCart(cart);
    }
  };

  deleteProductFromCart = (productToDelete) => {
    const { cart, items, user } = this.state;
    let productIndex = cart.productsToCart.findIndex(
      (cartProduct) => cartProduct.product.id === productToDelete.id
    );
    const cartQuantity = cart.productsToCart[productIndex].quantity;
    cart.totalPrice = +cart.totalPrice - +productToDelete.price * cartQuantity;
    cart.totalPrice = +cart.totalPrice < 0 ? 0 : cart.totalPrice;

    cart.productsToCart.splice(productIndex, 1);
    productIndex = items.findIndex(
      (originalProduct) => originalProduct.id === productToDelete.id
    );
    items[productIndex].stock += cartQuantity;

    this.setState({
      cart,
      items,
    });
    if (user) {
      cart.userId = user.id;
      this.saveCart(cart);
    }
  };

  saveCart = () => {
    this.postRequest(
      `${process.env.REACT_APP_API_URL}/cart`,
      this.state.cart
    ).then((result) => {});
  };

  toggleCart = () => {
    document.getElementById("cartContainer").classList.toggle("show");
  };

  toggleProductModal = (productSelected) => {
    this.setState({
      productSelected,
    });
    document.getElementById("productModal").classList.toggle("show");
  };

  toggleFinishPurchase = () => {
    document.getElementById("cartContainer").classList.toggle("show");
    document.getElementById("finishPurchaseModal").classList.toggle("show");
  };

  toggleLogin = () => {
    document.getElementById("loginContainer").classList.toggle("show");
  };

  toggleRegister = () => {
    document.getElementById("registerContainer").classList.toggle("show");
  };

  toggleProductForm = () => {
    document.getElementById("productFormContainer").classList.toggle("show");
  };

  toggleCategoryForm = () => {
    document.getElementById("categoryForm").classList.toggle("show");
  };

  toggleCategoriesMenu = () => {
    document.getElementById("allCategoriesContainer").classList.toggle("show");
  };

  toggleStockManagement = () => {
    document.getElementById("manageStockComponent").classList.toggle("show");
  };

  saveToken = (token) => {
    window.sessionStorage.removeItem(TOKEN_KEY);
    window.sessionStorage.setItem(TOKEN_KEY, token);
  };

  getToken = () => {
    return window.sessionStorage.getItem(TOKEN_KEY);
  };

  login = (email, password) => {
    this.postRequest(`${process.env.REACT_APP_API_URL}/login`, {
      email,
      password,
    })
      .then((result) => {
        if (result) {
          this.saveToken(result.data.accessToken);
          const user = jwtDecode(result.data.accessToken);
          this.setState({
            user,
            token: result.data.accessToken,
          });
          if (user.cart) {
            this.getRequest(
              `${process.env.REACT_APP_API_URL}/cart/${user.cart}`
            ).then((result) => {
              if (result) {
                this.setState({
                  cart: {
                    id: result.data.id,
                    totalPrice: +result.data.totalPrice,
                    userId: user.id,
                    isActive: true,
                    productsToCart: result.data.productsToCart,
                  },
                });
              }
            });
          }
          this.toggleLogin();
        }
      })
      .catch((error) => {
        window.alert("Se ha producido un error, pruebe más tarde");
        console.error(error);
      });
  };

  logout = () => {
    window.sessionStorage.clear();
    this.setState({
      user: null,
      token: null,
      cart: {
        id: null,
        totalPrice: 0.0,
        userId: null,
        isActive: true,
        productsToCart: [],
      },
    });
  };

  register = (name, email, password) => {
    this.postRequest(`${process.env.REACT_APP_API_URL}/users`, {
      name,
      email,
      password,
    })
      .then((result) => {
        if (result) {
          this.postRequest(`${process.env.REACT_APP_API_URL}/login`, {
            email,
            password,
          })
            .then((result) => {
              if (result) {
                this.saveToken(result.data.accessToken);
                this.setState({
                  user: jwtDecode(result.data.accessToken),
                  token: result.data.accessToken,
                });
                this.toggleRegister();
              }
            })
            .catch((error) => {
              window.alert("Se ha producido un error, pruebe más tarde");
              console.error(error);
            });
        }
      })
      .catch((error) => {
        window.alert("Se ha producido un error, pruebe más tarde");
        console.error(error);
      });
  };

  finishPurchase = () => {
    document.getElementById("finishPurchaseModal").classList.toggle("show");

    this.setState({
      cart: {
        id: null,
        totalPrice: 0.0,
        userId: null,
        isActive: true,
        productsToCart: [],
      },
    });
  };

  createProduct = (name, description, image, price, stock, categoryId) => {
    const { categories } = this.state;
    this.postRequest(`${process.env.REACT_APP_API_URL}/products`, {
      name,
      description,
      image,
      price,
      stock,
      category: categories.find(
        (categoryObject) => categoryObject.id === categoryId
      ),
    })
      .then((result) => {
        document.getElementById("product-image").value = "";
        document.getElementById("product-name").value = "";
        document.getElementById("product-description").value = "";
        document.getElementById("product-price").value = "";
        document.getElementById("product-stock").value = "";
        document.getElementById("product-categories").value = "";
        this.toggleProductForm();
        if (result) window.alert("Producto creado correctamente");
        else window.alert("Se ha producido un error, pruebe más tarde");
      })
      .catch((error) => {
        window.alert("Se ha producido un error, pruebe más tarde");
        console.error(error);
      });
  };

  createCategory = (name, image) => {
    this.postRequest(`${process.env.REACT_APP_API_URL}/categories`, {
      name,
      image,
    })
      .then((result) => {
        document.getElementById("category-image").value = "";
        document.getElementById("category-name").value = "";
        this.toggleCategoryForm();
        if (result) window.alert("Categoría creada correctamente");
        else window.alert("Se ha producido un error, pruebe más tarde");
      })
      .catch((error) => {
        window.alert("Se ha producido un error, pruebe más tarde");
        console.error(error);
      });
  };

  updateStock = (productId, stock) => {
    const product = this.state.items.find(
      (product) => product.id === productId
    );
    this.putRequest(
      `${process.env.REACT_APP_API_URL}/products/updateStock/${productId}`,
      {
        product,
        stock,
      }
    );
  };

  render() {
    const {
      error,
      isLoaded,
      itemsToShow,
      allItems,
      items,
      categories,
      hasMore,
      cart,
      productSelected,
      user,
    } = this.state;
    return (
      <div>
        <Nav
          isLoggedIn={user}
          isAdmin={user?.isAdmin}
          toggleCart={this.toggleCart}
          toggleLogin={this.toggleLogin}
          toggleRegister={this.toggleRegister}
          toggleProductForm={this.toggleProductForm}
          toggleCategoryForm={this.toggleCategoryForm}
          toggleStockManagement={this.toggleStockManagement}
          logout={this.logout}
        ></Nav>
        <FeaturedCategories
          categories={categories?.slice(0, 4)}
          filterByCategory={this.filterByCategory}
          toggleCategoriesMenu={this.toggleCategoriesMenu}
        ></FeaturedCategories>
        <div className="container mt-5">
          <div className="row-12 mt-5">
            {items?.length < allItems?.length && (
              <span
                className="delete-category-filter fw-bold"
                onClick={this.deleteCategoryFilter}
              >
                <i className="fas fa-trash me-2"></i>Eliminar filtro por
                categoría
              </span>
            )}
          </div>
        </div>
        <ProductsListContainer
          error={error}
          isLoaded={isLoaded}
          itemsToShow={itemsToShow}
          fetchMoreData={this.fetchMoreData}
          hasMore={hasMore}
          addProductToCart={this.addProductToCart}
          toggleProductModal={this.toggleProductModal}
        ></ProductsListContainer>
        <CartContainer
          addProductQuantity={this.addProductQuantity}
          decreaseProductQuantity={this.decreaseProductQuantity}
          deleteProductFromCart={this.deleteProductFromCart}
          totalPrice={cart.totalPrice}
          productsToCart={cart.productsToCart}
          toggleCart={this.toggleCart}
          toggleFinishPurchase={this.toggleFinishPurchase}
        ></CartContainer>
        <ProductDetail
          product={productSelected}
          toggleProductModal={this.toggleProductModal}
        ></ProductDetail>
        <FinishPurchaseModal
          price={cart.totalPrice}
          finishPurchase={this.finishPurchase}
        ></FinishPurchaseModal>
        <LoginComponent
          toggleLogin={this.toggleLogin}
          login={this.login}
        ></LoginComponent>
        <RegisterComponent
          toggleRegister={this.toggleRegister}
          register={this.register}
        ></RegisterComponent>
        <ProductFormComponent
          toggleProductForm={this.toggleProductForm}
          createProduct={this.createProduct}
          categories={categories}
        ></ProductFormComponent>
        <CategoryFormComponent
          createCategory={this.createCategory}
          toggleCategoryForm={this.toggleCategoryForm}
        ></CategoryFormComponent>
        <CategoriesMenuComponent
          toggleCategoriesMenu={this.toggleCategoriesMenu}
          categories={categories}
          filterByCategory={this.filterByCategory}
        ></CategoriesMenuComponent>
        <ManageStockComponent
          products={allItems}
          toggleStockManagement={this.toggleStockManagement}
          updateStock={this.updateStock}
        ></ManageStockComponent>
      </div>
    );
  }
}

export default App;
