import React, { Component } from "react";

class FinishPurchaseModal extends Component {
  render() {
    const { price, finishPurchase } = this.props;
    return (
      <div className="modal-container" id="finishPurchaseModal">
        <div className="modal-content w-50 h-75">
          <div className="close-label" onClick={finishPurchase}>
            <i className="fas fa-times" alt="Cerrar"></i>
          </div>
          <div className="container container h-100">
            <div className="row d-flex justify-content-evenly h-100">
              <div className="col-12">
                <h3 className="orange fw-bold">
                  Muchas gracias por comprar en PC-Store
                </h3>
              </div>
              <div className="col-12">
                <p className="fw-bold grey">
                  El total de su compra ha sido de {price.toFixed(2)} €
                </p>
              </div>
              <div className="col-12">
                <h4 className="orange fw-bold">
                  ¡Esperamos volver a verte pronto!
                </h4>
              </div>
              <div className="col-12">
                <button className="btn bg-orange w-100 text-black fw-bold">
                  Volver a la tienda
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default FinishPurchaseModal;
