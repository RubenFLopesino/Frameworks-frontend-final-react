import React, { Component } from "react";
import "./cart.css";
import CartRow from "./components/cart.row";

class Cart extends Component {
  addProductQuantity = (index) => {
    const { productsToCart } = this.props;

    this.props.addProductQuantity(productsToCart[index].product);
  };
  render() {
    const {
      totalPrice,
      productsToCart,
      addProductQuantity,
      decreaseProductQuantity,
      deleteProductFromCart,
      toggleCart,
      toggleFinishPurchase,
    } = this.props;
    return (
      <div className="cart" id="cartContainer">
        <div className="cart-detail">
          <div className="price-label">
            <span className="fw-bold">
              {`Total: ${Math.abs(totalPrice.toFixed(2))} €`}
            </span>
          </div>
          <div className="cart-close-label" onClick={toggleCart}>
            <i className="fas fa-times" alt="Cerrar"></i>
          </div>
        </div>
        {productsToCart.length > 0 && (
          <div className="mt-2 mb-2">
            <button
              className="btn bg-orange text-white fw-bold ms-2 w-100"
              onClick={toggleFinishPurchase}
            >
              Finalizar compra
            </button>
          </div>
        )}
        <div className="cart-rows-container">
          {productsToCart.map((productToCart) => (
            <CartRow
              key={productToCart.product.id}
              product={productToCart.product}
              quantity={productToCart.quantity}
              addProductQuantity={addProductQuantity}
              decreaseProductQuantity={decreaseProductQuantity}
              deleteProductFromCart={deleteProductFromCart}
            ></CartRow>
          ))}
        </div>
      </div>
    );
  }
}

export default Cart;
