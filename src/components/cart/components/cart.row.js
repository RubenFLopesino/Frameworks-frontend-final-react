import React, { Component } from "react";

class CartRow extends Component {
  addProductQuantity = () => {
    this.props.addProductQuantity(this.props.product);
  };
  decreaseProductQuantity = () => {
    this.props.decreaseProductQuantity(this.props.product);
  };
  deleteProductFromCart = () => {
    this.props.deleteProductFromCart(this.props.product);
  };

  state = {};
  render() {
    const { product, quantity } = this.props;
    return (
      <div className="cart-row mt-3">
        <div className="product-cart-data">
          <p className="product-cart-name">{product.name}</p>
          <div className="price-data">
            <span>Cantidad: {quantity}</span>
            <span>
              Precio total:
              {(product.price * quantity).toFixed(2)}
            </span>
          </div>
          <div className="quantity-controllers">
            <div className="quantity-buttons">
              <i
                className="fas fa-plus cursor-pointer me-2"
                onClick={this.addProductQuantity}
              ></i>
              <i
                className="fas fa-minus cursor-pointer"
                onClick={this.decreaseProductQuantity}
              ></i>
            </div>
            <div className="trash-button">
              <i
                className="fas fa-trash cursor-pointer"
                onClick={this.deleteProductFromCart}
              ></i>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default CartRow;
