import React, { Component } from "react";
import "./navbar.css";

class Nav extends Component {
  renderMenuButtons = () => {
    const {
      toggleCart,
      toggleLogin,
      toggleRegister,
      logout,
      toggleProductForm,
      toggleCategoryForm,
      toggleStockManagement,
    } = this.props;

    if (this.props.isLoggedIn && this.props.isAdmin)
      return (
        <div className="w-100 d-flex align-items-center justify-content-end">
          <button
            className="btn bg-orange text-white fw-bold me-2"
            onClick={toggleStockManagement}
          >
            Gestionar stock
          </button>
          <button
            className="btn bg-orange text-white fw-bold me-2"
            onClick={toggleProductForm}
          >
            Crear Producto
          </button>
          <button
            className="btn bg-orange text-white fw-bold me-2"
            onClick={toggleCategoryForm}
          >
            Crear Categoría
          </button>
          <button className="btn bg-orange text-white fw-bold" onClick={logout}>
            Cerrar sesión
          </button>
        </div>
      );
    else if (this.props.isLoggedIn)
      return (
        <div className="w-100 d-flex align-items-center justify-content-end">
          <button
            className="btn bg-orange text-white fw-bold me-2"
            onClick={logout}
          >
            Cerrar sesión
          </button>
          <button
            className="btn bg-orange text-white fw-bold"
            onClick={toggleCart}
          >
            Ver carrito
          </button>
        </div>
      );
    else
      return (
        <div className="w-100 d-flex align-items-center justify-content-end">
          <button
            className="btn bg-orange text-white fw-bold"
            onClick={toggleLogin}
          >
            Iniciar sesión
          </button>
          <button
            className="btn bg-orange text-white fw-bold ms-2"
            onClick={toggleRegister}
          >
            Registrarse
          </button>
          <button
            className="btn bg-orange text-white fw-bold ms-2 d-none d-lg-block"
            onClick={toggleCart}
          >
            Ver mi carrito
            <i className="fas fa-shopping-cart"></i>
          </button>
        </div>
      );
  };
  render() {
    return (
      <nav className="navbar navbar-dark bg-dark fixed-top fw-bold navbar-expand-lg">
        <div className="container-fluid">
          <a className="navbar-brand" href="/">
            <img src={`${process.env.PUBLIC_URL}/icon.png`} alt="Site logo" />
          </a>
          <button
            className="navbar-toggler"
            type="button"
            data-bs-toggle="collapse"
            data-bs-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <div className="d-flex">{this.renderMenuButtons()}</div>
          </div>
        </div>
      </nav>
    );
  }
}

export default Nav;
