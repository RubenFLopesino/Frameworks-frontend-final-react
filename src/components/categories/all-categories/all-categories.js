import React, { Component } from "react";
import "./all-categories.css";
class CategoriesMenuComponent extends Component {
  filterByCategory = (event) => {
    this.props.filterByCategory(event.target.getAttribute("id"), true);
  };
  render() {
    const { toggleCategoriesMenu, categories, filterByCategory } = this.props;
    return (
      <div className="all-categories pt-5" id="allCategoriesContainer">
        <div className="categories-menu">
          <div className="close-label" onClick={toggleCategoriesMenu}>
            <i className="fas fa-times" alt="Cerrar"></i>
          </div>
        </div>
        <div>
          <ul>
            {categories?.map((category) => (
              <li
                key={category.id}
                id={category.id}
                className="category-link fw-bold"
                onClick={this.filterByCategory}
              >
                {category.name}
              </li>
            ))}
          </ul>
        </div>
      </div>
    );
  }
}

export default CategoriesMenuComponent;
