import React, { Component } from "react";

class CategoryCard extends Component {
  filterByCategory = (event) => {
    const { category, filterByCategory } = this.props;
    event.preventDefault();
    filterByCategory(category.id);
  };
  render() {
    const { category } = this.props;
    return (
      <a
        className="featured-categories-link fw-bold"
        onClick={this.filterByCategory}
      >
        <img src={category?.image} />
        {category?.name}
      </a>
    );
  }
}

export default CategoryCard;
