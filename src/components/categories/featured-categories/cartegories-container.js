import React, { Component } from "react";
import "./categories.css";
import CategoryCard from "./components/featured-category-card";
class FeaturedCategories extends Component {
  render() {
    const { categories, filterByCategory, toggleCategoriesMenu } = this.props;
    return (
      <div className="container mt-5 pt-5">
        <div className="row mt-5 mb-5">
          <div className="col-12">
            <h1 className="orange-underline mb-3">Categorías destacadas</h1>
          </div>
          {categories.map((category) => (
            <div className="col-sm-6 col-md-6 col-xl-3" key={category.id}>
              <CategoryCard
                category={category}
                filterByCategory={filterByCategory}
              ></CategoryCard>
            </div>
          ))}
          <div className="col-12 mt-5">
            <p className="featured-text fw-bold" onClick={toggleCategoriesMenu}>
              <i className="fas fa-bars me-2"></i>Ver todas las categorías
            </p>
          </div>
        </div>
      </div>
    );
  }
}

export default FeaturedCategories;
