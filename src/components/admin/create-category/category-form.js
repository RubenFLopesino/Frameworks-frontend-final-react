import React, { Component } from "react";
class CategoryFormComponent extends Component {
  createCategory = (event) => {
    event.preventDefault();
    const imageInput = document.getElementById("category-image");
    var reader = new FileReader();
    if (imageInput.files.length > 0) {
      reader.readAsDataURL(imageInput.files[0]);
      reader.onload = async () => {
        const name = document.getElementById("category-name").value;
        const image = reader.result;
        if (name !== "" && image !== "") this.props.createCategory(name, image);
        else window.alert("Por favor rellene todos los campos del formulario");
      };
    } else window.alert("Por favor rellene todos los campos del formulario");
  };
  render() {
    const { toggleCategoryForm } = this.props;
    return (
      <div className="modal-container" id="categoryForm">
        <div className="modal-content h-50 w-50 d-flex align-items-center justify-content-center">
          <form>
            <div className="close-label w-100" onClick={toggleCategoryForm}>
              <i className="fas fa-times" alt="Cerrar"></i>
            </div>
            <div className="mb-3">
              <label htmlFor="category-name" className="form-label">
                Nombre de la categoría:
              </label>
              <input
                type="text"
                className="form-control"
                id="category-name"
                name="name"
              />
            </div>
            <div className="mb-3">
              <label htmlFor="category-image" className="form-label">
                Imagen:
              </label>
              <input className="form-control" type="file" id="category-image" />
            </div>
            <button
              type="submit"
              className="btn bg-orange w-100 text-black fw-bold"
              onClick={this.createCategory}
            >
              Crear categoría
            </button>
          </form>
        </div>
      </div>
    );
  }
}

export default CategoryFormComponent;
