import React, { Component } from "react";

class ProductFormComponent extends Component {
  createProduct = (event) => {
    event.preventDefault();
    const imageInput = document.getElementById("product-image");
    var reader = new FileReader();
    if (imageInput.files.length > 0) {
      reader.readAsDataURL(imageInput.files[0]);
      reader.onload = async () => {
        const name = document.getElementById("product-name").value;
        const description = document.getElementById(
          "product-description"
        ).value;
        const image = reader.result;
        const price = document.getElementById("product-price").value;
        const stock = document.getElementById("product-stock").value;
        const category = document.getElementById("product-categories").value;

        if (
          name !== "" &&
          description !== "" &&
          image !== "" &&
          price !== "" &&
          stock !== "" &&
          category !== ""
        )
          this.props.createProduct(
            name,
            description,
            image,
            price,
            stock,
            category
          );
        else window.alert("Por favor rellene todos los campos del formulario");
      };
    } else window.alert("Por favor rellene todos los campos del formulario");
  };
  render() {
    const { toggleProductForm, categories } = this.props;

    return (
      <div className="modal-container" id="productFormContainer">
        <div className="modal-content h-100 w-50 d-flex align-items-center justify-content-center">
          <form className="mt-5 mb-5">
            <div className="close-label w-100" onClick={toggleProductForm}>
              <i className="fas fa-times" alt="Cerrar"></i>
            </div>
            <div className="mb-1">
              <label htmlFor="product-name" className="form-label">
                Nombre del producto:
              </label>
              <input
                type="text"
                className="form-control"
                id="product-name"
                name="name"
              />
            </div>
            <div className="mb-1">
              <label htmlFor="product-description" className="form-label">
                Descripción del producto:
              </label>
              <textarea
                type="text"
                className="form-control"
                id="product-description"
                name="description"
              ></textarea>
            </div>
            <div className="mb-1">
              <label htmlFor="product-image" className="form-label">
                Imagen del producto:
              </label>
              <input className="form-control" type="file" id="product-image" />
            </div>
            <div className="mb-1">
              <label htmlFor="product-price" className="form-label">
                Precio:
              </label>
              <input
                type="number"
                step="any"
                className="form-control"
                id="product-price"
                name="price"
              />
            </div>
            <div className="mb-1">
              <label htmlFor="product-stock" className="form-label">
                Stock:
              </label>
              <input
                type="number"
                className="form-control"
                id="product-stock"
                name="stock"
              />
            </div>

            <div className="mb-1">
              <label htmlFor="product-stock" className="form-label">
                Categoría:
              </label>
              <select
                className="form-select"
                id="product-categories"
                name="category"
              >
                {categories?.map((category) => (
                  <option key={category.id} value={category.id}>
                    {category.name}
                  </option>
                ))}
              </select>
            </div>
            <button
              className="btn bg-orange w-100 text-black fw-bold"
              type="submit"
              onClick={this.createProduct}
            >
              Crear producto
            </button>
          </form>
        </div>
      </div>
    );
  }
}

export default ProductFormComponent;
