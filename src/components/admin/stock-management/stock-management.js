import React, { Component } from "react";

class ManageStockComponent extends Component {
  onChange = (event) => {
    const productId = event.target.getAttribute("id");
    const stock = event.target.value;
    this.props.updateStock(productId, stock);
  };
  render() {
    const { products, toggleStockManagement } = this.props;
    return (
      <div className="modal-container" id="manageStockComponent">
        <div className="stock-management h-75 w-50 p-4 overflow-auto bg-white">
          <div className="close-label w-100" onClick={toggleStockManagement}>
            <i className="fas fa-times" alt="Cerrar"></i>
          </div>
          <div className="d-flex justify-content-center align-items-center">
            <div className="container">
              <div className="row d-flex justify-content-evenly h-100">
                {products?.map((product) => (
                  <div
                    className="col-12 mt-2 mb-2 d-flex align-items-center w-100"
                    key={product.id}
                  >
                    <div className="d-block w-50">
                      <p className="fw-bold grey">{product.name}</p>
                    </div>
                    <div className="d-block w-50">
                      <input
                        type="number"
                        id={product.id}
                        defaultValue={product.stock}
                        onChange={this.onChange}
                      />
                    </div>
                  </div>
                ))}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default ManageStockComponent;
