import React from "react";
import ReactDOM from "react-dom";
import "/node_modules/bootstrap/dist/css/bootstrap.min.css";
import "/node_modules/bootstrap/dist/js/bootstrap.min";
import "./style/style.css";
import App from "./components/App";

ReactDOM.render(<App></App>, document.getElementById("root"));
